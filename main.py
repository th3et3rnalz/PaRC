# In the line of code below, the WorkServer is also started
from MainNode import t_webapp
from MainNode.WorkServer.helpers import print_update

# Let's get everything up and running:
print_update('WebServer starting')
t_webapp.start()
