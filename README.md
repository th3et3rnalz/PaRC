# Project status: deprecated.

Currently working on firmware v2.0 with:
* improved web interface for monitoring state and operating the cluster.
* Python3 library for writing code to run on the cluster (with integrated local-machine testing)
* Improved workflow for extracting results from cluster.
