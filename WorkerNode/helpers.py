from termcolor import cprint
from subprocess import Popen, PIPE
import os
import time
from enum import Enum


# The Colours chosen in this helpers file is to help differentiate with the MainNode updates (which come on the same
# terminal)
def print_status(text, start=True, color='white'):
    print("n[  ", end="")  # n from node
    if start:
        cprint("START", color='cyan', end="")
    else:
        cprint(" DONE", color='cyan', end="")
    print("  ]", end="  ")
    cprint(text, color=color)


def print_error(text, color='white'):
    print("n[  ", end="")  # n from node
    cprint("ERROR", color='magenta', end="")
    print("  ]", end="  ")
    cprint(text, color=color)


def run_on_sis(cmd):
    return Popen(cmd, stdout=PIPE).stdout.read().decode('utf8').strip()


def save_file(value, filename, byte=True):
    # print('os_base', os.path.abspath(__file__))
    # base_path = os.path.abspath(__file__).replace('BackgroundWorker.py', 'storage')
    base_path = '/home/pi/Cluster/WorkerNode/storage'
    # first we must check that the file does not yet exist
    if filename in os.listdir(base_path):
        return 'file exists'
    file_path = base_path + '/' + filename

    if byte:
        f = open(file_path, 'wb')
    else:
        f = open(file_path, 'w')
    f.write(value)
    f.close()
    return True


class MsgLevel(Enum):
    INFO = 1
    ERROR = 2


class Message:
    """
    This instances of this class will hold information that is to be returned to the main node.
    The reason to implement a class rather than just storing the raw values is that I want to enforce a few extra
    variable that I also want stored, such as time, level, and maybe others later.

    The list of instances of this class will act as a logbook. Note that I chose this over the logging value, because
    (as far as I know) sending information from that library over the network is a bit of pain (and I would need to
    add to it from which node the info comes from, ...)
    """
    def __init__(self, value, level, msg_type='msg', log_time=None):
        """

        :type level: MsgLevel
        """
        self.value = value
        self.level = level
        if log_time is None:
            self.time = time.asctime()
        else:
            self.time = log_time
        self.type = msg_type

    def get_data(self):
        if self.level == MsgLevel.ERROR:
            level = 'ERROR'
        elif self.level == MsgLevel.INFO:
            level = 'INFO'
        else:
            raise NotImplementedError
        return {'value': str(self.value), 'level': level, 'time': str(self.time), 'type': self.type}
