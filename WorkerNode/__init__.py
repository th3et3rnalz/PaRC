"""
This code is never to be run on the same device as the rest of this. It is here to simplify testing. It's purpose is to
hold the code that is to be sent over to the worker nodes, as I want them to require as little (manual) setup as
possible.
"""
from WorkerNode.helpers import run_on_sis
from WorkerNode.BackgroundWorker import BackgroundThread

main_node_ip = open('main_node_ip.txt', 'r').read().rstrip()

background_thread = BackgroundThread(main_node_ip=main_node_ip,
                                     get_work_task_path="/nodes/get_work_task",
                                     get_work_project_path="/nodes/get_work_project",
                                     post_status_path='/nodes/post_status',
                                     post_answer_path='/nodes/post_answer',
                                     authentication=None,
                                     node_name=run_on_sis('hostname'),
                                     port=5000)

background_thread.loop()
