"""
This independent thread will keep on looping, until it sees that a file (named with its hostid) in the "stop" folder.
"""
import os
import sys
import time
from subprocess import Popen, PIPE
from requests import get, post
from WorkerNode.helpers import save_file, run_on_sis, Message, MsgLevel
from importlib import import_module
import site
from zipfile import ZipFile
from requests.exceptions import ConnectionError
from multiprocessing import Process


class BackgroundThread:
    def __init__(self, main_node_ip, get_work_task_path, get_work_project_path, post_status_path,
                 post_answer_path, authentication, node_name, port=None):
        self.hostid = Popen(['hostid'], stdout=PIPE).stdout.read().decode('utf8').replace("\n", '')
        self.stop_loop = False
        self.max_processes = 4
        self.main_node_ip = main_node_ip

        self.get_work_task_path = get_work_task_path
        self.get_work_project_path = get_work_project_path
        self.post_status_path = post_status_path
        self.post_answer_path = post_answer_path

        self.auth = authentication
        self.node_name = node_name
        self.port = port
        self.messages = []  # This will hold instances of the Message class, and will be returned in "post_status()"
        self.active_processes = []
        site.addsitedir('/home/pi/Cluster/WorkerNode/storage')
        self.stdout = open('stdout.txt', 'w')
        sys.stdout = self.stdout
        self.start_time = time.time()

    def check_stop(self):
        if "stop" not in os.listdir('/home/pi/Cluster'):
            os.mkdir('/home/pi/Cluster/stop')
        if self.hostid in os.listdir('/home/pi/Cluster/stop'):
            self.stop_loop = True
            self.stdout.close()  # this closes the log file.
            # we should then send it over to the main node for logging
            # TODO: implement return of stdout to the main node
            # The software then stops itself.

    def loop(self):
        """
        This loop is just a holding function, when the node is waiting for work.
        :return: None
        """
        while not self.stop_loop:
            if len(self.active_processes) < self.max_processes:
                self.check_for_work()

            time.sleep(5)

            self.post_status()

            self.check_stop()

    def download_project(self, project_name):
        url = "http://" + self.main_node_ip + self.get_work_project_path + '/' + project_name
        if self.port is not None:
            url = "http://" + self.main_node_ip + ":" + str(self.port) + self.get_work_project_path \
                  + '/' + project_name
        try:
            response = get(url)

            filename = response.headers['filename']
            save_file(value=response.content,
                      filename=filename,
                      byte=True)

            # Let's unzip it:
            os.mkdir(f'WorkerNode/storage/{filename.replace(".zip", "")}')
            zip = ZipFile(f'WorkerNode/storage/{filename}', 'r')
            zip.extractall(f'WorkerNode/storage/{filename.replace(".zip", "")}')
            zip.close()

            # and then we must delete the zip file.
            os.system(f'rm WorkerNode/storage/{filename}')
            folder_name = filename.replace('.zip', '')

            # However, downloading the project is not the only thing that is to be done to get the task ready to be run.
            # We must also install the required software for that project.
            out = Popen(['python3', '-m', 'pip', 'install', '-r', f'WorkerNode/storage/{folder_name}/requirements.txt'],
                        stdout=PIPE).stdout.read()
            if out[:5] == 'ERROR':  # That means that (most probably) the project does not have an requirements.txt file
                self.messages.append(Message(value=f'File not found: requirement.txt for {project_name}',
                                             level=MsgLevel.ERROR))

        except ConnectionError:
            self.messages.append(Message(value=f"Project '{project_name}' could not be downloaded",
                                         level=MsgLevel.ERROR))

    def check_for_work(self):
        url = "http://" + self.main_node_ip + self.get_work_task_path
        if self.port is not None:
            url = "http://" + self.main_node_ip + ":" + str(self.port) + self.get_work_task_path
        try:
            response = get(url)

            if response.headers['Content-Type'] == 'application/json':
                task_info = response.json()

                # First we must see if we have the project in our storage:
                if task_info.get('project_name') not in os.listdir('/home/pi/Cluster/WorkerNode/storage'):
                    self.download_project(task_info.get('project_name'))

                # Now that we have it, let's run out task:
                process = Process(target=self.run_task,
                                  kwargs={'module_name': task_info.get('project_name'),
                                          'input_val': task_info.get('input_parameters'),
                                          'task_info': task_info},
                                  name=str(task_info.get('id')))

                self.active_processes.append(process)

                self.messages.append(Message(value=f"About to start working on task {task_info.get('id')} with params: "
                                                   f"{task_info.get('input_parameters')}",
                                             level=MsgLevel.INFO))

                process.start()

            elif response.headers['Content-Type'].split(';')[0] == 'text/html':
                # this means we got a message that there is no work
                self.messages.append(Message(value="No extra work",
                                             level=MsgLevel.INFO))
            else:
                self.messages.append(Message(value="Unexpected response for check_for_work endpoint: "
                                                   f"{response.headers['Content-Type']}",
                                             level=MsgLevel.ERROR))
        except ConnectionError:
            if time.time() > self.start_time + 20:
                self.messages.append(Message(value='Cannot get work: ConnectionError',
                                             level=MsgLevel.ERROR))
            else:
                pass
                # This means that the webserver isn't running yet because the MainServer is still setting up the other
                # nodes. Note that the timing of 20 seconds was chosen completely arbitrarily and may need to be
                # increased.

    def post_status(self):
        # Emptying the list of processes to remove the inactive ones.
        # This makes sure that we are able to take on new tasks when the machine can do it.
        for p in self.active_processes:
            if not p.is_alive():
                self.messages.append(Message(value=f'Dead task:{p.name}',
                                             level=MsgLevel.INFO))
                self.active_processes.pop(self.active_processes.index(p))

        system_info = {'cpu_temp': int(run_on_sis(['cat', '/sys/class/thermal/thermal_zone0/temp'])) / 1000,
                       'gpu_temp': float(run_on_sis(['vcgencmd', 'measure_temp']).split('=')[1].split("'")[0])}

        self.messages.append(Message(value=system_info,
                                     level=MsgLevel.INFO,
                                     msg_type='system'))

        thread_processing = len(self.active_processes)

        self.messages.append(Message(value=thread_processing,
                                     level=MsgLevel.INFO,
                                     msg_type='threads'))

        url = "http://" + self.main_node_ip + self.post_status_path
        if self.port is not None:
            url = "http://" + self.main_node_ip + ":" + str(self.port) + self.post_status_path
        try:
            payload = [msg.get_data() for msg in self.messages]
            # TODO: implement better structure for
            self.messages = []

            r = post(url=url,
                     data={'node_name': self.node_name,
                           'status': str(payload)})

            if r.status_code != 200:
                self.messages.append(Message(value=f'Post normal status wrong status code: {r.status_code}',
                                             level=MsgLevel.ERROR))
            else:
                # Since the message got through well, let's empty the payload:
                self.messages = []
        except ConnectionError:
            if time.time() > self.start_time + 20:
                self.messages.append(Message(value='Cannot post status: ConnectionError',
                                             level=MsgLevel.ERROR))

    def run_task(self, module_name, task_info, input_val='initial'):
        old_dir = os.getcwd()
        os.chdir(f'/home/pi/Cluster/WorkerNode/storage/{module_name}')

        module_path = f'{module_name}.__main__'
        module = import_module(module_path)

        func = module.task.target
        if input_val == 'initial':
            input_val = module.task.initial

        if 'outdir' not in os.listdir():
            os.mkdir('outdir')
        run_on_sis(['python3', '-m', 'pip', 'install', '-r',
                    f'/home/pi/Cluster/WorkerNode/storage/{module_name}/requirements.txt'])

        try:
            res = func(input_val)

            f = open('results.txt', 'a')
            f.write(str(res))
            f.write('\n')
            f.close()

            task_info['result'] = str(res)
            self.post_answer(task_info)

        except Exception as e:  # This means that the code is faulty
            self.messages.append(Message(value=f'Running {module_name} with inputs {str(input_val)} gave error: {e}',
                                         level=MsgLevel.ERROR))
            self.post_status()

        os.chdir(old_dir)

    def post_answer(self, task_info):
        url = "http://" + self.main_node_ip + self.post_answer_path
        if self.port is not None:
            url = "http://" + self.main_node_ip + ":" + str(self.port) + self.post_answer_path

        self.messages.append(Message(value=task_info.get('result'),
                                     level=MsgLevel.ERROR))

        r = post(url=url,
                 data={'task_id': task_info.get('id'),
                       'result': task_info.get('result')})
        if r.status_code != 200:
            self.messages.append(Message(value=f'Post answer wrong status code: {r.status_code}',
                                         level=MsgLevel.ERROR))
