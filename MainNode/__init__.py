"""
This is the main run_file of the computer. We need to first get the web server thread running, and then pass the floor
to the MainServer __init__ file.
"""
import threading
from MainNode.WebServer import app
import MainNode.WorkServer

t_webapp = threading.Thread(target=app.run,
                            kwargs={'host': '0.0.0.0',
                                    'port': 5000,
                                    'debug': False},
                            name='WebApp')