from MainNode.WorkServer.helpers import TaskHolder
import site
from queue import Queue
from importlib import import_module
from MainNode.WorkServer.task_helpers import *


class TaskManager:
    def __init__(self, max_queue_size):
        self.task_storage_dir = 'MainNode/WorkServer/TaskStorage/Raw'
        self.all_projects = []
        self.max_size = max_queue_size
        self.task_number_counter = 1

        self.to_do_queue = Queue(maxsize=max_queue_size)
        self.doing_dict = {}
        self.done_dict = {}

        site.addsitedir(self.task_storage_dir)
        self.create_tasks()

    def create_tasks(self):
        # First we must find all projects
        all_project_names = os.listdir(self.task_storage_dir)
        for module_name in all_project_names:
            self.all_projects.append(import_module(module_name + ".__main__"))

        # Now that we have all the projects, let's start creating some tasks:
        for project in self.all_projects:
            tasks = project.create_tasks()
            self.add_tasks(tasks, project, name="ConnectFourDistributedTraining")

    def add_tasks(self, task_dict_list, project, name):
        for task_dict in task_dict_list:
            task = TaskHolder(id=self.task_number_counter,
                              project=project,
                              name=name,
                              input_parameters=task_dict)
            self.task_number_counter += 1
            self.to_do_queue.put(task)

    def get_task_for_doing(self):
        if self.to_do_queue.qsize() > 0:
            task = self.to_do_queue.get()
            self.doing_dict[task.id] = task
            return task
        return None

    def set_answer(self, task_id, answer):
        task = self.doing_dict[task_id]
        del self.doing_dict[task_id]
        task.result = answer
        self.done_dict[task_id] = task
        task.save_func(task_id, answer)
        self.analyze_answers()

    def analyze_answers(self):
        print(self.done_dict)
