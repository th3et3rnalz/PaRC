from termcolor import cprint
from subprocess import Popen, PIPE


def print_status(text, start=True, color='white', indent=0):
    print(f"{'  '*indent}m[  ", end="")
    if start:
        cprint("START", color='green', end="")
    else:
        cprint("DONE ", color='green', end="")
    print("  ]", end="  ")
    cprint(text, color=color)



def print_error(text, color='white', indent=0):
    print(f"{'  '*indent}m[  ", end="")  # m from main
    cprint("ERROR", color='red', end="")
    print("  ]", end="  ")
    cprint(text, color=color)


def print_update(text, color='white', indent=0):
    print(f"{'  '*indent}m[  ", end="")  # m from main
    cprint("STATUS", color='blue', end="")
    print("  ]", end="  ")
    cprint(text=text, color=color)


class TaskHolder:
    def __init__(self, id, input_parameters, project, name):
        self.id = id
        self.name = name
        self.result = None
        self.input_parameters = input_parameters
        self.save_func = project.task.save_answer

    def get_dict(self):
        return {'id': self.id,
                'project_name': self.name,
                'input_parameters': self.input_parameters}


def get_ip():
    p = Popen(['hostname', '-i'], stdout=PIPE)
    ip = p.stdout.read().decode('utf8').split(" ")[0]
    return ip