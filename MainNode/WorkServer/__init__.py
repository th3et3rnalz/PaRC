"""
This piece of code will be response for tying all parts of the master node together,
including, but not limited to:
    * Running the WebApp
    * Sending Commands to all the nodes
    * Providing one or a few states of interest
"""
from csv import DictReader
from MainNode.WorkServer.MainNodeClass import MainNode
from MainNode.WorkServer.helpers import get_ip, print_update
import atexit
import shutil


def start_server():
    print("\n\n========== PARC is starting up ==========", end='\n')
    print(" PARC stands for PortAble Raspberry pi Cluster.\n"
          " The 'm' at the start of a message stands for \n"
          " main, and 'n' for node", end='\n\n\n')

    print_update('WorkServer starting')

    shutil.make_archive("MainNode/WorkServer/TaskStorage/Zip/ConnectFourDistributedTraining",
                        'zip',
                        "MainNode/WorkServer/TaskStorage/Raw/ConnectFourDistributedTraining")

    node_list = [row for row in DictReader(open('MainNode/WorkServer/node_list.csv'))]
    m = MainNode(node_list, ip=get_ip())
    m.connect_all_nodes()
    m.check_network()
    # m.update_and_upgrade_all()
    m.initialize_all(force_update=True)

    def exit_handler():
        m.soft_exit_all_nodes()
        print("========== PARC stopped ==========", end='\n')
    atexit.register(exit_handler)

    return m


m = start_server()
