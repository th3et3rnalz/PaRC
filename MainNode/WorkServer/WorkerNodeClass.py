from fabric import Connection
from fabric.transfer import Transfer
import shutil
import time
import threading
from invoke.exceptions import UnexpectedExit
from MainNode.WorkServer.helpers import print_update


class WorkerNode:
    """
    This class is the representation of the Worker on the Main node. It holds all the data regarding each specific node.
    """
    def __init__(self, connect_dict, main_node_ip):
        self.ip = connect_dict['ip']
        self.port = connect_dict['port']
        self.user = connect_dict['user']
        self.password = connect_dict['password']
        self.c = Connection(host=self.ip,
                            user=self.user,
                            port=self.port)
        self.t = Transfer(self.c)
        self.main_node_ip = main_node_ip
        self.id = self.c.run('hostid', hide='out').stdout.rstrip()
        self.name = self.ip.replace('.local', '')

        self.speed_test = None
        self.status = {'system': None, 'n_threads_running': 0, 'last_seen': None}
        self.thread = None

    def update_and_upgrade(self):
        """
        This will send a command to the node to update and upgrade the host os.
        Note that this is completely separate from the project and is simply a general tool.
        This code was tested for Raspbian os Nodes (which runs on Raspberry Pi's).
        """
        self.c.sudo('apt-get update')
        self.c.sudo('apt-get upgrade -y')

    def initialize(self, force_update=False):
        """
        This function is run just after the connection has been established. It's goal is to verify that the required
        files are up and running on the nodes.
        :return: None
        """
        # Only if the node has not yet been setup, do we need to set it up. hence:
        folders_in_home = self.c.run('cd ~&&ls', hide='out').stdout.split("\n")
        if "Cluster" not in folders_in_home:
            self.c.run("mkdir Cluster", hide='out')
            self.install_base_software()

        if ('WorkerNode' not in self.c.run('cd /home/pi/Cluster&&ls', hide='out').stdout.split('\n')) or\
                (force_update is True):
            self.soft_stop()
            try:
                self.c.run('cd /home/pi/Cluster&&rm -r WorkerNode', hide='out')
            except UnexpectedExit:  # the WorkerNode folder may not always be there
                pass

            # Then we (locally) zip up the WorkerNode class:
            shutil.make_archive('WorkerNode', 'zip', 'WorkerNode')
            if 'WorkerNode' not in self.c.run('ls /home/pi/Cluster/',  hide='out').stdout.split('\n'):
                self.c.run('mkdir /home/pi/Cluster/WorkerNode', hide='out')

            # And finally put it on the server
            self.t.put(local='WorkerNode.zip', remote='/home/pi/Cluster/WorkerNode')
            print_update(f'{self.name}: Zip File pushed', color='white', indent=1)

            self.c.run(f"cd /home/pi/Cluster&&echo '{self.main_node_ip}' > main_node_ip.txt", hide='out')

            # And unzip it.
            self.c.run('cd /home/pi/Cluster/WorkerNode&&unzip -u WorkerNode.zip', hide='out')

            # Now we need to add the starting file:
            self.c.put(local='start_node.py', remote='/home/pi/Cluster/')

        self.thread = threading.Thread(target=self.c.run,
                                       kwargs={'command': 'cd /home/pi/Cluster&&python3 start_node.py',
                                               'hide': None},
                                       name=self.ip.replace('.local', ''))
        self.run()

    def __repr__(self):
        return f"<WorkerNode id={self.id} n={self.name}>"

    def run(self):
        # First we must find out if the stop folder exists
        if 'stop' in self.c.run('ls /home/pi/Cluster', hide='out').stdout.split("\n"):
            self.c.run('cd /home/pi/Cluster/stop&&rm $(hostid)', hide='out')
        self.thread.start()
        print_update(f'{self.name}: Initialized', color='white', indent=1)

    def soft_stop(self):
        if "stop" not in self.c.run("cd /home/pi/Cluster&&ls", hide='out').stdout.split('\n'):
            self.c.run('cd /home/pi/Cluster&&mkdir stop', hide='out')
        self.c.run("cd /home/pi/Cluster/stop&&touch $(hostid)", hide='out')

    def update_status(self, status_dict, value):
        if value == 'speed_test':
            self.speed_test = status_dict
        elif value == 'system':
            self.status['system'] = status_dict
        elif value == 'threads':
            self.status['threads'] = status_dict
        self.status['last_seen'] = time.time()

    def install_base_software(self):
        self.c.run('sudo apt-get install iperf3 -y')
        self.c.run('sudo apt-get install libatlas-base-dev -y')  # this solves some obscure issue with numpy
        self.c.run('sudo apt-get install python3-dev python3-pip -y')
        self.c.run('sudo apt install libopenblas-dev libblas-dev m4 cmake cython python3-dev python3-yaml '
                   'python3-setuptool -y')  # these are requirements for pytorch
