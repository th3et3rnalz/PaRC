from MainNode.WorkServer.WorkerNodeClass import WorkerNode
from MainNode.WorkServer.helpers import print_status
from invoke.exceptions import UnexpectedExit
from subprocess import Popen, PIPE
from MainNode.WorkServer.taskcreator import TaskManager
import logging

logging.basicConfig(filename='MainNodeLog.txt', level=logging.DEBUG)


class MainNode:
    def __init__(self, expected_nodes_list, ip, max_queue_size=1000):
        self.expected_nodes_list = expected_nodes_list
        self.nodes = []
        self.ip = ip
        self.task_manager = TaskManager(max_queue_size=max_queue_size)

    def connect_all_nodes(self):
        for node in self.expected_nodes_list:
            c = WorkerNode(node, self.ip)
            logging.debug(f'Connected node {c.name}')
            self.nodes.append(c)

    def update_and_upgrade_all(self):
        for node in self.nodes:
            node.update_and_upgrade()
            logging.debug(f"Updating and upgrading node {node.name}")

    def initialize_all(self, force_update=False):  # TODO: find better name for function
        print_status('Node Startup', start=True)
        for node in self.nodes:
            node.initialize(force_update=force_update)
            logging.debug(f'Initialized node {node.name}')
        print_status('Node Startup', start=False)

    def find_new_nodes(self):
        raise NotImplementedError

    def check_network(self, ping_count=1, speed_count=1):
        print_status("Network Analysis", start=True, color='white', indent=0)
        # We must start a server on the main_node, and one after the other, connect the worker_nodes to it.
        # For all the network tests, we will need an iperf3 server, so let's start it:
        logging.debug("Starting network testing")
        iperf3_server = Popen(['iperf3', '-s'], stdout=PIPE)

        for node in self.nodes:
            # Before we do anything, we must see that iperf3 is installed on the node.
            try:
                logging.debug(f"Starting iperf3 server on node {node.name}")
                node.c.run('iperf3 -v', hide='out')
            except UnexpectedExit:
                logging.warning(f"Node {node.name} did not have iperf3 installed, so doing that now")
                node.c.run('sudo apt-get install iperf3 -y')

            print(f"\t\t\t- Node {node.name}")
            # We start with the ping test
            logging.debug(f"Stating network testing for node {node.name}")
            p = Popen(["ping", f'-c {ping_count}', node.ip], stdout=PIPE)
            raw = p.stdout.read().decode('utf8')
            cleaned = raw.split("\n")[-2].split(" = ")[-1][:-3]
            p_min, p_avg, p_max, p_sd = cleaned.split("/")

            # Then we get the speed test going with iperf. The server for this was already started on the master nodes.
            # Now we get the data from the client:
            speed_raw = node.c.run(f"iperf3 -c {self.ip} -t {speed_count}", hide="out")
            sender, receiver = speed_raw.stdout.split("\n")[-5:-3]
            sender_speed = sender.split("MBytes ")[-1][:17].rstrip().replace(" ", "")
            receiver_speed = receiver.split("MBytes ")[-1][:17].rstrip().replace(" ", "")

            # Now that we have all the data, we should put it into our "node"
            result = {'ping': {'p_min': float(p_min),
                               'p_avg': float(p_avg),
                               'p_max': float(p_max),
                               'p_sd': float(p_sd)},
                      'speed': {'sender': sender_speed.replace("MBits/sec", ""),
                                'receiver': receiver_speed.replace("MBits/sec", "")}}

            node.speed_test = result
            logging.debug(f"Finished network testing for node {node.name}")

        # Now that we're done testing everything, let's stop the iperf server:
        iperf3_server.terminate()
        print_status("Network Analysis", start=False, color='white', indent=0)
        logging.debug("Finished network testing")

    def get_status_info(self):
        status_info = {}
        for node in self.nodes:
            temp = {'speed_test': node.speed_test,
                    'hostid': node.id,
                    'colour': 'PaleVioletRed'}

            status_info[node.name] = temp

        print_status(str(status_info))
        return status_info

    def update_status(self, info_dict):
        for node in self.nodes:
            if node.name == info_dict.get('node_name').replace('.local', ''):
                payload = info_dict.get('status')
                if payload is not None:
                    # This will be a list, so we must iterate over it:
                    for data in eval(payload):
                        if data['type'] in ['system', 'threads', 'speed_test']:
                            node.update_status(data['value'], data['type'])
                        elif data['type'] == 'msg':
                            print_status(data)
                return True
        return False

    def soft_exit_all_nodes(self):
        logging.debug("Soft exiting the system. Bye Felicia")
        [w.soft_stop() for w in self.nodes]

    # Interface with the TaskManager Class
    def get_task_for_doing(self):
        return self.task_manager.get_task_for_doing()

    def set_answer(self, task_id, answer):
        return self.task_manager.set_answer(task_id=task_id, answer=answer)

    def get_node(self, node_name):
        """This function will return the node instance with the same name as given, else return None"""
        # To do this, we must loop over the nodes:
        for node in self.nodes:
            if node.name == node_name:
                return node
        return None


