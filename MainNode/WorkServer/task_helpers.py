"""
This file is supposed to be a copy of the file that every project must have in order to run
properly on the cluster. This ensures a common interface between cluster and project, as
well as making the optimizer choice more simple, as their code is all in this single file.

Note that the __main__ file of a project should contain a variable called task, that is
an instance of the RemoteTask class.

Version: 1.0
"""

import os


class RemoteTask:
    def __init__(self, name, target, parameters, initial, returns, optimizer, requirements=None):
        self.name = name
        self.target = target
        self.parameters = parameters
        self.initial = initial
        self.returns = returns
        self.optimizer = optimizer
        self.requirements = requirements

    def run(self, param_dict):
        # We must first check that the parameters are correct.
        try:
            assert type(param_dict) == dict
            for param in self.parameters:
                assert param.name in param_dict
        except AssertionError:
            return Exception("Incorrect parameters")
        if self.requirements is not None:
            path = os.path.abspath(__file__).replace('helpers.py', self.requirements)
            os.system(f'python3 -m pip install -r {path}')
        return self.target(param_dict)


class NumericalParameter:
    def __init__(self, name, min_val, max_val, optimize=True):
        self.type = 'NumericalParameter'  # Python quirks ...
        self.name = name
        self.min = min_val
        self.max = max_val
        self.optimize = optimize


class BooleanParameter:
    def __init__(self, name, optimize=True):
        self.type = 'BooleanParameter'  # Python quirks ...
        self.name = name
        self.optimize = optimize  # If optimize is set to false, the value will not be changed from the initial value


class SimpleOptimize:
    def __init__(self, min_max, n_divisions, parameters, initial, percentage_step=0.5):
        self.min_max = min_max
        self.n_divisions = n_divisions  # This is the number of subdivisions per parameter. Note that this results in
        # n_divisions^n_parameters times the function has to be run.
        self.parameters = parameters
        self.initial = initial
        self.p_step = percentage_step

    def create_task_dicts(self, up=True):
        """
        :param up: Boolean representing whether the algorithm should explore up or down
        :return: [TaskHolder]
        """
        tasks = [self.initial]

        for p in self.parameters:
            if p.type == "NumericalParameter":
                base_parameters = self.initial
                base_val = base_parameters[p.name]
                if up:
                    val = base_val + self.p_step*(p.max - base_val)
                else:
                    val = base_val - self.p_step * (base_val - p.min)
                base_parameters[p.name] = val
                tasks.append(base_parameters)
            elif p.type == "BooleanParameter":
                if p.optimize:
                    # With a boolean parameter, the only option we have is to flip it:
                    base_parameters = self.initial
                    base_parameters[p.name] = not base_parameters[p.name]
                    tasks.append(base_parameters)
        return tasks
