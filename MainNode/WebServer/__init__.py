import os

from flask import Flask, render_template, request, make_response, send_from_directory, abort, jsonify

from MainNode.WorkServer import m
from MainNode.WorkServer.helpers import print_error
import logging

logging.basicConfig(filename='MainNodeLog.txt', level=logging.DEBUG)

app = Flask(__name__)


@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html", title="PC - Home")


@app.route("/tasks")
def tasks():
    return render_template("tasks.html", title="PC - Tasks")


@app.route("/status")
def status():
    return render_template("status.html", title="PC - Status")


@app.route('/api/status')
def api_status():
    return jsonify(m.get_status_info())


@app.route('/nodes/get_work_project/<string:project_name>')
def get_work_project(project_name):
    try:
        filename = project_name + '.zip'
        directory = os.getcwd() + "/MainNode/WorkServer/TaskStorage/Zip"
        response = make_response(send_from_directory(directory=directory,
                                                     filename=filename,
                                                     as_attachment=True), 200)
        response.headers['filename'] = filename
        return response
    except FileNotFoundError:
        print_error(f"A Node tried getting: {project_name}, but the file could not be found")
        abort(404)


@app.route('/nodes/get_work_task')
def get_work_task():
    work_task = m.get_task_for_doing()
    logging.debug('WHOOP WHOOP')
    if work_task is None:
        return make_response("no_work", 200)
    else:
        return make_response(jsonify(work_task.get_dict()), 200)


@app.route('/nodes/post_status', methods=['POST'])
def post_status():
    data = request.form
    data = {key: data[key] for key in data.keys()}  # Transforming from ImmutableDict to Dict
    m.update_status(data)
    return make_response(jsonify({'status': 'ok'}), 200)


@app.route('/nodes/post_answer', methods=['POST'])
def post_result():
    data = request.form
    data = {key: data[key] for key in data.keys()}

    f = open('results.txt', 'a')
    f.write(str(data.get('result') + '\n'))
    f.close()

    logging.debug("RESULTS:" + str(data.get('result')))
    try:
        print_error('whoop whoop: ' + str(data.get('result'))[:100])
    except IndexError:
        print_error('whoop whoop: ' + str(data.get('result')))
    m.set_answer(task_id=int(data.get('task_id')), answer=data.get('result'))
    return make_response(jsonify({'status': 'ok'}), 200)
