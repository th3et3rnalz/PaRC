

function create_card_with_canvas(id=null, title='AddTitle') {
    let card = document.createElement('div');
    card.classList.add('card');


    let canvas = document.createElement('canvas');
    canvas.style.height = '100%';
    canvas.style.width = '100%';
    if (id != null){
        canvas.setAttribute('id', id);
    }

    // chartjs wants the canvas element to be inside a div (for proper sizing):
    let container = document.createElement('div');
    container.style.height = '100%';
    container.style.width = '100%';


    container.appendChild(canvas);
    card.appendChild(container);
    document.getElementById('content').appendChild(card);
}

onload_queue = Array();

function run_onload(){
    for (let i=0; i < onload_queue.length; i++){
        onload_queue[i]();
    }
}

window.onload = run_onload;

function transform_node_data() {
    // This function will transform the window.node data from being node centric to being data category centric. This
    // will make it much easier to create all the graphs.
    const data = window.node_info;
    console.log("Starting to transform the data:");
    console.log(data);

    // The code below will perform a deep copy of the placeholder object. This is so I can easily add more parameters.
    let ping_arr = Array();
    let speed_arr = Array();
    let base_data = {'colours': Array(), 'names': Array()};

    const node_names = Object.keys(data)

    for (let node_nr=0; node_nr < node_names.length; node_nr++){
        const node_name = node_names[node_nr];
        const node = data[node_names[node_nr]];
        const node_colour = node['colour'];

        base_data['colours'].push(node_colour);
        base_data['names'].push(node_name);

        ping_arr.push(node['speed_test']['ping']['p_avg']);
        speed_arr.push(node['speed_test']['speed']['receiver']);
    }

    window.graph_data = {'ping': ping_arr, 'speed': speed_arr, 'base_data': base_data};
}