function create_charts(node_info){
    window.node_info = node_info;
    console.log(node_info);

    // This is where the code should be added to do the plotting
    transform_node_data();
    let speed_chart = make_speed_chart();
    let ping_chart = make_ping_chart();
    window.chart = ping_chart;

    // const speed_height = speed_chart.parentNode.parentNode.style.height;
    // ping_chart.parentNode.parentNode.style.height = speed_height;
}


function status_onload(){
    console.log("status.js's onload function was called");

    // This will get the data from the /api/status endpoint and give it to the create_charts function.
    fetch('/api/status').then(response => response.json()).then(json => create_charts(json));
}

onload_queue.push(status_onload);