function close_nav(){
    console.log("Closing Navigation");
    const slide_out = document.getElementById("nav_rounded_container");
    slide_out.style.marginLeft = '-100%';
}

function open_nav (){
    console.log("Opening Navigation");
    const slide_out = document.getElementById("nav_rounded_container");
    slide_out.style.marginLeft = '0';
}


function base_onload(){
    console.log("base.js's onload function was called");
    const open_nav_button = document.getElementById("mobile_nav_bars");
    open_nav_button.addEventListener('click', open_nav);

    const close_nav_button = document.getElementById("mobile_nav_times");
    close_nav_button.addEventListener('click', close_nav);
}

onload_queue.push(base_onload);