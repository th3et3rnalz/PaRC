function make_ping_chart(id='ping_chart'){
    create_card_with_canvas(id, "ping");
    // return true
    const ctx = document.getElementById(id).getContext('2d');

    const data = window.graph_data['ping'];
    const base_info = window.graph_data['base_data'];

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: base_info['names'],
            datasets: [{
                label: 'ping [ms]',
                data: data,
                backgroundColor: base_info['colours']
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            title: {
                text: 'Ping',
                display: true,
                fontSize: 30
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
    return myChart
}

function make_speed_chart(id='speed_chart'){
    create_card_with_canvas(id, "speed");
    // return true
    const ctx = document.getElementById(id).getContext('2d');

    // return false;

    const data = window.graph_data['speed'];
    const base_info = window.graph_data['base_data'];

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: base_info['names'],
            datasets: [{
                label: 'Speed [MBits/sec]',
                data: data,
                backgroundColor: base_info['colours']
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            title: {
                text: 'Speed',
                display: true,
                fontSize: 30
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
    return myChart
}